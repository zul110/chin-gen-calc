const chinGenTypeSelector = $('#chin-gen-calc-type-selector');

const chinGenDaySelector1 = $('#chin-gen-calc-1-day-selector');
const chinGenMonthSelector1 = $('#chin-gen-calc-1-month-selector');
const chinGenYearSelector1 = $('#chin-gen-calc-1-year-selector');

const chinGenDaySelector2 = $('#chin-gen-calc-2-day-selector');
const chinGenMonthSelector2 = $('#chin-gen-calc-2-month-selector');
const chinGenYearSelector2 = $('#chin-gen-calc-2-year-selector');

const chinGenResultBody = $('#chin-calc-result-body');
const chinGenResultContainer = $('.chin-calc-result-bg-content-container');
const chinGenResultIcon = $('#chin-calc-result-icon');
const chinGenResultTable = $('#chin-calc-result-table-container');

const chinGenSubmitButton = $('#btn_Submit_ChinGenCalc_Right');
const chinGenResultButton = $('#chin-calc-result-return-button');

const chinGenTable = [
    ['Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Girl', 'Boy', 'Boy'],
    ['Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl'],
    ['Boy', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Girl'],
    ['Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Boy', 'Girl', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Girl', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy', 'Boy', 'Girl', 'Girl', 'Girl'],
    ['Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Boy', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Boy'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Girl', 'Boy', 'Boy'],
    ['Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy', 'Girl', 'Girl', 'Boy', 'Boy'],
    ['Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy'],
    ['Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl'],
    ['Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Girl', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy'],
    ['Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl'],
    ['Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Boy'],
    ['Boy', 'Boy', 'Girl', 'Boy', 'Boy', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Girl'],
    ['Girl', 'Boy', 'Boy', 'Girl', 'Girl', 'Girl', 'Boy', 'Girl', 'Boy', 'Girl', 'Boy', 'Boy']
];

const chinGenResultText = {
    Boy: 'وفق الجدول الصيني فأنك حامل بولد!',
    Girl: 'وفق الجدول الصيني فأنك حامل ببنت!'
};

const chinGenCalcInit = () => {
    chinGenDaySelector1.val(0).change();
    chinGenMonthSelector1.val(0).change();
    chinGenYearSelector1.val(0).change();

    chinGenMonthSelector1.prop('disabled', true);
    chinGenDaySelector1.prop('disabled', true);

    chinGenDaySelector2.val(0).change();
    chinGenMonthSelector2.val(0).change();
    chinGenYearSelector2.val(0).change();

    chinGenMonthSelector2.prop('disabled', true);
    chinGenDaySelector2.prop('disabled', true);

    const chinGenUpdateYears = () => {
        const nextYear = moment().add(1, 'year').format('YYYY');
        const currentYear = moment().format('YYYY');
        const lastYear = moment().subtract(1, 'year').format('YYYY');

        const years = [nextYear, currentYear, lastYear];
        const bdayYearsStart = +moment().subtract(45, 'years').format('YYYY');
        const bdayYearsEnd = +moment().subtract(18, 'years').format('YYYY');

        const bdayYears = [];

        let currentBdayYear = bdayYearsStart;
        while(currentBdayYear <= bdayYearsEnd) bdayYears.push(currentBdayYear++);

        chinGenYearSelector1.innerHTML = years.forEach(year => chinGenYearSelector1.append(new Option(year, year)));

        chinGenYearSelector2.innerHTML = bdayYears.forEach(year => chinGenYearSelector2.append(new Option(year, year)));
    };

    const chinGenUpdateMonths = () => {
        const months = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
        ];

        chinGenMonthSelector1.innerHTML = months.forEach(month => chinGenMonthSelector1.append(new Option(moment(month, 'M').format('MMMM'), month)));

        chinGenMonthSelector2.innerHTML = months.forEach(month => chinGenMonthSelector2.append(new Option(moment(month, 'M').format('MMMM'), month)));
    };

    const chinGenUpdateDays = () => {
        const days1 = [];
        const days2 = [];

        const numDaysInMonth1 = moment(`${chinGenYearSelector1.val()}-${chinGenMonthSelector1.val()}`, 'YYYY-MM').daysInMonth();

        const numDaysInMonth2 = moment(`${chinGenYearSelector2.val()}-${chinGenMonthSelector2.val()}`, 'YYYY-MM').daysInMonth();

        let day = 1;
        while (day <= numDaysInMonth1) {
            days1.push(day++);
        }

        day = 1;
        while (day <= numDaysInMonth2) {
            days2.push(day++);
        }

        chinGenDaySelector1.innerHTML = days1.forEach(day => chinGenDaySelector1.append(new Option(day, day)));

        chinGenDaySelector2.innerHTML = days2.forEach(day => chinGenDaySelector2.append(new Option(day, day)));
    };

    chinGenUpdateYears();
    chinGenUpdateMonths();

    chinGenYearSelector1.on('change', e => {
        const year = e.target.value;
        const month = chinGenMonthSelector1.val();

        let _year = year;

        chinGenMonthSelector1.prop('disabled', year == 0);
        chinGenDaySelector1.prop('disabled', year == 0 || month == 0);
    });

    chinGenYearSelector2.on('change', e => {
        const year = e.target.value;
        const month = chinGenMonthSelector2.val();

        let _year = year;

        chinGenMonthSelector2.prop('disabled', year == 0);
        chinGenDaySelector2.prop('disabled', year == 0 || month == 0);
    });

    chinGenMonthSelector1.on('change', e => {
        const year = chinGenYearSelector1.val();
        const month = e.target.value;

        let _month = month;

        chinGenUpdateDays();

        chinGenDaySelector1.prop('disabled', year == 0 || month == 0);
    });

    chinGenMonthSelector2.on('change', e => {
        const year = chinGenYearSelector2.val();
        const month = e.target.value;

        let _month = month;

        chinGenUpdateDays();

        chinGenDaySelector2.prop('disabled', year == 0 || month == 0);
    });

    chinGenDaySelector1.on('change', e => {
        const day = chinGenDaySelector1.val();

        let _day = day;
    });

    chinGenDaySelector2.on('change', e => {
        const day = chinGenDaySelector2.val();

        let _day = day;
    });
};

chinGenSubmitButton.click(() => {
    const type = chinGenTypeSelector.val();

    const year1 = chinGenYearSelector1.val();
    const month1 = chinGenMonthSelector1.val();
    const day1 = chinGenDaySelector1.val();

    const year2 = chinGenYearSelector2.val();
    const month2 = chinGenMonthSelector2.val();
    const day2 = chinGenDaySelector2.val();

    if (
        (!type)
        ||
        ((!year1 || !month1 || !day1) || (year1 == 0 || month1 == 0 || day1 == 0))
        ||
        ((!year2 || !month2 || !day2) || (year2 == 0 || month2 == 0 || day2 == 0))
    ) {
        chinGenHideResults();
    } else {
        chinGenShowResults();

        const motherAge = type == 0 ? moment(`${year1}-${month1}-${day1}`, 'YYYY').diff(moment(`${year2}-${month2}-${day2}`, 'YYYY'), 'years') : moment(`${year1}-${month1}-${day1}`, 'YYYY').diff(moment(`${year2}-${month2}-${day2}`, 'YYYY').add(1, 'year'), 'years');

        const conceptionMonth = type == 0 ? moment(`${year1}-${month1}-${day1}`, 'YYYY-MM-DD').format('M') : moment(`${year1}-${month1}-${day1}`, 'YYYY-MM-DD').subtract(9, 'months').format('M');

        
        const gender = chinGenTable[motherAge - 18][conceptionMonth - 1];

        if(gender === 'Boy') {
            chinGenResultContainer.removeClass('female');
            chinGenResultContainer.addClass('male');

            chinGenResultIcon.removeClass('female');
            chinGenResultIcon.addClass('male');
        }  else {
            chinGenResultContainer.removeClass('male');
            chinGenResultContainer.addClass('female');

            chinGenResultIcon.removeClass('male');
            chinGenResultIcon.addClass('female');
        }

        chinGenResultBody.html(chinGenResultText[gender]);

        chinGenUpdateTable(motherAge - 18, conceptionMonth - 1);
    }
});

const chinGenUpdateTable = (motherAge, conceptionMonth) => {
    chinGenResultTable.empty();
    let tableHtml = `<table>`;

    tableHtml += '<tr>';
    for(let i = 0; i <= 12; i++) {
        if(i === 0) tableHtml += `<td class='chin-calc-table-col'></td>`;
        else tableHtml += `<td class='chin-calc-table-col'>${i}</td>`;
    }
    tableHtml += '</tr>';

    chinGenTable.forEach((row, rowIndex) => {
        tableHtml += '<tr>';

        tableHtml += `<td class='chin-calc-table-col'>${rowIndex + 18}</td>`;

        row.forEach((col, colIndex) => {
            tableHtml += `<td class='chin-calc-table-col${col === "Boy" ? " chin-calc-table-col-male" : " chin-calc-table-col-female"}${rowIndex == motherAge && colIndex == conceptionMonth ? " chin-calc-table-col-result" : ""}'></td>`;
        });

        tableHtml += `<td class='chin-calc-table-col'></td>`;

        tableHtml += '</tr>';
    })

    tableHtml += `</table>`;

    chinGenResultTable.html(tableHtml);
}

chinGenResultButton.click(() => {
    chinGenHideResults();
});

const chinGenHideResults = () => {
    $('.chin-calc-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const chinGenShowResults = () => {
    $('.chin-calc-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};